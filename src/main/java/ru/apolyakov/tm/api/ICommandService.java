package ru.apolyakov.tm.api;

import ru.apolyakov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
